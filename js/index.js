

$(document).ready(function(){
    $(".slick-carousel").slick({
        nav:true,
        dots: true,
        });

});



function rightDropdownmenu(){
    let openMenu = $("#profile-item-btn");
    let b = $('.dropdown-items');
    if (b.hasClass('active')) {
        openMenu.removeClass('active');
        b.removeClass('active');

    }else{
        openMenu.addClass('active');
        b.addClass('active')
    }
}

document.getElementById('profile-item-btn').onclick = rightDropdownmenu;

function userDropdown() {
    let openMenu = $("#user-dropdown");
    let dropdownText =document.getElementById('dropdownText');
    let activeObj ={usrDropdown:openMenu, triangle:$(".triangle"),circle:$(".circle"),btnDrop:$(".btn-dropdown"),options:$(".options"), optionsCont:$('.options-cont') };

    if(openMenu.hasClass('active')){
        for (let remove in activeObj){
            activeObj[remove].removeClass('active')
        }
        dropdownText.innerHTML= 'Dropdown Normal'
    }else{
        for(let add in activeObj){
            activeObj[add].addClass('active')
        }
        dropdownText.innerHTML='Dropdown Active'
    }
}

document.getElementById('user-dropdown').onclick= userDropdown;


var d = 1;
function calendarDays() {
    let days = document.getElementById('days');
    let newLi = document.createElement('li');
    let newSpan = document.createElement('span');
    newLi.className='days-list';
    newSpan.className= 'active';

    if (d !== 16){

        days.appendChild(newLi, days.children[d]);
        newLi.innerHTML= d ;
        d++;

    }else{
        days.appendChild(newLi, days.children[d]);
        newLi.appendChild(newSpan, days.children[16])
        newSpan.innerHTML= d;
        d++
    }


}
while (d <=31){
    calendarDays();
}

$(document).ready( function() {
    $( "#range1" ).slider({
        range: true,
        min: 0,
        max: 500,
        values: [ 75, 300 ],
        slide: function( event, ui ) {
            $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
        }
    });
    $( "#amount" ).val( "$" + $( "#range1" ).slider( "values", 0 ) +
        " - $" + $( "#range1" ).slider( "values", 1 ) );
} );

$( function() {
    $( "#range2" ).slider({
        range: "min",
        value: 37,
        min: 1,
        max: 700,
        slide: function( event, ui ) {
            $( "#amount" ).val( "$" + ui.value );
        }
    });
    $( "#amount" ).val( "$" + $( "#range2" ).slider( "value" ) );
} );